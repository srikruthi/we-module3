import random

CARD_VALUES = {'2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8, '9': 9, '10': 10, 'J': 11, 'Q': 12, 'K': 13, 'A': 14}

def create_deck():
    deck = []
    for suit in ['C', 'H', 'S']:
        for value in CARD_VALUES:
            deck.append(value + suit)
    return deck

def shuffle_deck(deck):
    random.shuffle(deck)

def determine_winner(player_bids, card):
    winning_bid = max(player_bids, key=lambda x: CARD_VALUES.get(x[0]))
    max_bids = [bid for bid in player_bids if bid[0] == winning_bid[0]]
    winning_score = CARD_VALUES[card[0]] / len(max_bids)
    return {player: winning_score for player in max_bids}

def play_game():
    deck = create_deck()
    shuffle_deck(deck)

    player1_score = 0
    player2_score = 0

    while deck:
        card = deck.pop()
        print("Diamond card on auction:", card)
        
        player_bids = {}
        for player in ['Player 1', 'Player 2']:
            bid = input(f"{player}, choose a card to bid (2-9,10,J,Q,K,A): ").upper()
            while bid not in CARD_VALUES:
                print("Invalid bid. Please choose a valid card.")
                bid = input(f"{player}, choose a card to bid (2-9,10,J,Q,K,A): ").upper()
            player_bids[bid] = player

        winning_bids = determine_winner(player_bids, card)
        for bid, player in winning_bids.items():
            if player == 'Player 1':
                player1_score += bid
            else:
                player2_score += bid
        
        print("Player 1 score:", player1_score)
        print("Player 2 score:", player2_score)

    if player1_score > player2_score:
        print("Player 1 wins the game!")
    elif player1_score < player2_score:
        print("Player 2 wins the game!")
    else:
        print("It's a tie!")

def main():
    play_game()

if __name__ == "__main__":
    main()
